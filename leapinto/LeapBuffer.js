var LeapBuffer = 
{
	ws: null,
	lowres: true, // this floors all the position and velocity numbers to provide whole integers, speed at the expense of resolution
	bufferlimit: 16,
	
	parseData: function(obj)
	{
		if('pointables' in obj)
		{
			if(obj.pointables.length > 0) LeapBuffer.parsePointables(obj);
			else if(LeapBuffer.leapdata.pointables.length > 0) LeapBuffer.leapdata.pointables = [];
		}
		if('hands' in obj)
		{
			if(obj.hands.length > 0) LeapBuffer.parseHands(obj);
			else if(LeapBuffer.leapdata.hands.length > 0) LeapBuffer.leapdata.hands = [];
		}
	},
	
	
	/*
	
	
		CODE THAT DEALS WITH THE HANDS
	
	*/
	
	parseHands: function(obj)
	{
		// loop through existing hands first and determine if updates need to be made, or if the pointable should be removed from the buffer
		for(var i = 0; i < LeapBuffer.leapdata.hands.length; i++)
		{
			var result = LeapUtility.checkForHandInNewData(obj, LeapBuffer.leapdata.hands[i]);
			if(result >= 0)
			{
				// update current item in buffer
				LeapBuffer.updateHandInBuffer(LeapBuffer.leapdata.hands[i], obj.hands[result]);
				// then destroy item from new data object
				obj.hands.splice(result, 1);
			}
			else
			{
				LeapBuffer.destroyHandInBuffer(i);
				// destroy item from buffer, finger is gone 
			}
		}
		
		// once we have cleaned up the buffer, add any new pointers to the buffer
		for(i = 0; i < obj.hands.length; i++)
		{
			// for each pointable left, create a new object in the buffer
			LeapBuffer.addHandToBuffer(obj.hands[i]);
		}
	},
	
	addHandToBuffer: function(objhand)
	{
		var position, velocity;
		if(LeapBuffer.lowres)
		{
			position = [Math.floor(objhand.palmPosition[0]), Math.floor(objhand.palmPosition[1]), Math.floor(objhand.palmPosition[2])];		
			velocity = [Math.floor(objhand.palmVelocity[0]), Math.floor(objhand.palmVelocity[1]), Math.floor(objhand.palmVelocity[2])];
		}
		else
		{
			position = [objhand.palmPosition[0], objhand.palmPosition[1], objhand.palmPosition[2]];		
			velocity = [objhand.palmVelocity[0], objhand.palmVelocity[1], objhand.palmVelocity[2]];
		}
		var hand = 
		{
			id: objhand.id,
			palmPosition : [
				[0, 0, 0]
			],
			palmVelocity : [
				[0, 0, 0]
			],
			bufferSize: 1	
		};
		LeapBuffer.leapdata.hands.push(hand);
	},
	
	updateHandInBuffer: function(bufferhand, objhand)
	{
		var position, velocity;
		if(LeapBuffer.lowres)
		{
			position = [Math.floor(objhand.palmPosition[0]), Math.floor(objhand.palmPosition[1]), Math.floor(objhand.palmPosition[2])];		
			velocity = [Math.floor(objhand.palmVelocity[0]), Math.floor(objhand.palmVelocity[1]), Math.floor(objhand.palmVelocity[2])];
		}
		else
		{
			position = [objhand.palmPosition[0], objhand.palmPosition[1], objhand.palmPosition[2]];		
			velocity = [objhand.palmVelocity[0], objhand.palmVelocity[1], objhand.palmVelocity[2]];
		}
		
		bufferhand.palmPosition.unshift(position);
		bufferhand.palmVelocity.unshift(velocity);
		bufferhand.bufferSize ++;
		
		if(bufferhand.bufferSize > LeapBuffer.bufferlimit)
		{
			bufferhand.palmPosition.pop();
			bufferhand.palmVelocity.pop();
			bufferhand.bufferSize = LeapBuffer.bufferlimit;
		}
	},
	
	destroyHandInBuffer: function(index)
	{
		LeapBuffer.leapdata.hands.splice(index, 1);
	},
	
	/*
	
	
		CODE THAT DEALS WITH THE POINTABLES
	
	*/
	parsePointables: function(obj)
	{
		// loop through existing pointables first and determine if updates need to be made, or if the pointable should be removed from the buffer
		for(var i = 0; i < LeapBuffer.leapdata.pointables.length; i++)
		{
			var result = LeapUtility.checkForPointableInNewData(obj, LeapBuffer.leapdata.pointables[i]);
			if(result >= 0)
			{
				// update current item in buffer
				LeapBuffer.updatePointableInBuffer(LeapBuffer.leapdata.pointables[i], obj.pointables[result]);
				obj.pointables.splice(result, 1);
				// then destroy item from new data object
			}
			else
			{
				LeapBuffer.destroyPointableInBuffer(i);
				// destroy item from buffer, finger is gone 
			}
		}
		
		// once we have cleaned up the buffer, add any new pointers to the buffer
		for(i = 0; i < obj.pointables.length; i++)
		{
			// for each pointable left, create a new object in the buffer
			LeapBuffer.addPointableToBuffer(obj.pointables[i]);
		}
	},

	addPointableToBuffer: function(objpointable)
	{
		var position = [objpointable.tipPosition[0], objpointable.tipPosition[1], objpointable.tipPosition[2]];		
		var velocity = [objpointable.tipVelocity[0], objpointable.tipVelocity[1], objpointable.tipVelocity[2]];	
		var pointable = 
		{
			handId: objpointable.handId,
			id: objpointable.id,
			tipPosition : [
				[0, 0, 0]
			],
			tipVelocity : [
				[0, 0, 0]
			],
			bufferSize: 1	
		};
		LeapBuffer.leapdata.pointables.push(pointable);
	},
	
	updatePointableInBuffer: function(bufferpointable, objpointable)
	{
		
		var position = [objpointable.tipPosition[0], objpointable.tipPosition[1], objpointable.tipPosition[2]];		
		var velocity = [objpointable.tipVelocity[0], objpointable.tipVelocity[1], objpointable.tipVelocity[2]];	
		
		bufferpointable.tipPosition.unshift(position);
		bufferpointable.tipVelocity.unshift(velocity);
		bufferpointable.bufferSize ++;
		
		if(bufferpointable.bufferSize > LeapBuffer.bufferlimit)
		{
			bufferpointable.tipPosition.pop();
			bufferpointable.tipVelocity.pop();
			bufferpointable.bufferSize = LeapBuffer.bufferlimit;
		}
	},
	
	destroyPointableInBuffer: function(index)
	{
		LeapBuffer.leapdata.pointables.splice(index, 1);
	},
	
	
	initLeap: function() 
	{
	  //Create and open the socket
	  LeapBuffer.ws = new WebSocket("ws://localhost:6437/");
	  
	  // On successful connection
	  LeapBuffer.ws.onopen = function(event) {};
	  
	  // On message received
	  LeapBuffer.ws.onmessage = function(event) {
	    var obj = JSON.parse(event.data);
	    LeapBuffer.parseData(obj);
	  };
	  
	  // On socket close
	  LeapBuffer.ws.onclose = function(event) { LeapBuffer.ws = null; }
	  
	  //On socket error
	  LeapBuffer.ws.onerror = function(event) { alert("Received error"); };
	},
	
	
	
	
	leapdata:
	{
		/*
			
		
			array of detected pointables updated with every new socket data event
			for each pointable in the new data, if the handId and id combo doesn't match any existing element: create new object
			if it does exist, add values to buffer array in pointables object
			if id in buffer pointables array does not match anything in new data, wipe if from the buffer
		*/
		hands: [ ],
		pointables : [ ]
	}
}

var LeapUtility =
{
	checkForPointableInBuffer: function(id)
	{
		for(var i = 0; i < LeapBuffer.leapdata.pointables.length; i++)
		{
			if(LeapBuffer.leapdata.pointables[i].id === id) return true;
		}
		return false;
	},
	checkForPointableInNewData: function(obj, pointable)
	{
		for(var i = 0; i < obj.pointables.length; i++)
		{
			if(obj.pointables[i].id === pointable.id) return i;
		}
		return -1;
	},
	checkForHandInNewData: function(obj, hand)
	{
		for(var i = 0; i < obj.hands.length; i++)
		{
			if(obj.hands[i].id === hand.id) return i;
		}
		return -1;
	},
	getPointableById: function(pointables, id)
	{
		
		for(var i = 0; i < pointables.length; i++)
		{
			if(pointables[i].id === Number(id)) return i;
		}
		return -1;
	}
}