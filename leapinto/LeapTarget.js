var LeapTarget = 
{
	dropTargets: [],
	catchTargets: [],
	pokeTargets: [],
	dragTargets: [],
	
	newDropTarget: function(node)
	{
		LeapTarget.dropTargets.push(node);
	},
	removeDropTarget: function(node)
	{
			
	},
	newPokeTarget: function(node)
	{
		LeapTarget.pokeTargets.push(node);
	},
	newCatchTarget: function(node)
	{
		LeapTarget.catchTargets.push(node);
	},
	newDragTarget: function(node)
	{
		LeapTarget.dragTargets.push(node);
	},
	
	checkForCatchCollision: function(draggable)
	{		
		var rx, ry, rw, rh;
		
		var x = draggable.position().left;
		var y = draggable.position().top;
		var z = draggable.attr('z');
		// CHECK CATCH TARGETS
		for(var i = 0; i < LeapTarget.catchTargets.length; i++)
		{
			rx = LeapTarget.catchTargets[i].position().left;
			ry = LeapTarget.catchTargets[i].position().top;
			rw = LeapTarget.catchTargets[i].width();
			rh = LeapTarget.catchTargets[i].height();
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh)
			{
				LeapTarget.catchTargets[i].trigger({
					type: "leapcatch",
					message: "Leap Catch",
					draggable: draggable
				});
				return true;
			}
		}
		
	},
	checkForDropCollision: function(draggable)
	{		
		var rx, ry, rw, rh;
		
		var x = draggable.position().left;
		var y = draggable.position().top;
		var z = draggable.attr('z');
		// CHECK CATCH TARGETS
		for(var i = 0; i < LeapTarget.dropTargets.length; i++)
		{
			rx = LeapTarget.dropTargets[i].position().left;
			ry = LeapTarget.dropTargets[i].position().top;
			rw = LeapTarget.dropTargets[i].width();
			rh = LeapTarget.dropTargets[i].height();
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh)
			{
				LeapTarget.dropTargets[i].trigger({
					type: "leapdrop",
					message: "Leap Drop",
					draggable: draggable
				});
				return true;
			}
		}
		
	},
	
	/*
		checkForHitAtPosition is used to determine how to set the state of the pointable based on interaction
	*/
	
	checkForHitAtPosition: function(pointable)
	{		
		var rx, ry, rw, rh, rz;
		
		var x = pointable.screenX;
		var y = pointable.screenY;
		var z = pointable.screenZ;
		
		// CHECK DRAG TARGETS FIRST
		// todo: write in way to switch between the ability to catch multiple draggables under one pointable and a single draggable only.
		for(var i = 0; i < LeapTarget.dragTargets.length; i++)
		{			
			rx = LeapTarget.dragTargets[i].position().left;
			ry = LeapTarget.dragTargets[i].position().top;
			rw = LeapTarget.dragTargets[i].width();
			rh = LeapTarget.dragTargets[i].height();
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh)
			{	
				// if this is the first hit, set a few attributes to help LeapDisplay
				if(!LeapTarget.dragTargets[i].hasClass('dragging') && !LeapTarget.dragTargets[i].hasClass('flicking'))
				{
					LeapTarget.dragTargets[i].trigger({
						type: "leapdrag",
						message: "Leap Drag Start",
						draggable: LeapTarget.dragTargets[i],
						pointable: pointable
					});
					LeapTarget.dragTargets[i].addClass('dragging');
					// hitx and hity act as anchor points for the dragging
					LeapTarget.dragTargets[i].attr('hitx', x - rx).attr('hity', y - ry).attr('hitz', z);
					LeapTarget.dragTargets[i].attr('pid', pointable.id);
				}
				if(!LeapTarget.dragTargets[i].hasClass('flicking') && !LeapTarget.dragTargets[i].hasClass('coasting')) LeapTarget.dragTargets[i].attr('vx', pointable.tipVelocity[0][0]).attr('vy', pointable.tipVelocity[0][1]).attr('vz', pointable.tipVelocity[0][2]);
				return true;
			}
		}
		
		// CHECK DROP TARGETS
		for(var i = 0; i < LeapTarget.dropTargets.length; i++)
		{
			rx = LeapTarget.dropTargets[i].position().left;
			ry = LeapTarget.dropTargets[i].position().top;
			rw = LeapTarget.dropTargets[i].width();
			rh = LeapTarget.dropTargets[i].height();
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh) return true;
		}
		
		// CHECK CATCH TARGETS
		for(var i = 0; i < LeapTarget.catchTargets.length; i++)
		{
			rx = LeapTarget.catchTargets[i].position().left;
			ry = LeapTarget.catchTargets[i].position().top;
			rw = LeapTarget.catchTargets[i].width();
			rh = LeapTarget.catchTargets[i].height();
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh) return true;
		}
		
		// CHECK POKE TARGETS
		for(var i = 0; i < LeapTarget.pokeTargets.length; i++)
		{
			rx = LeapTarget.pokeTargets[i].position().left;
			ry = LeapTarget.pokeTargets[i].position().top;
			rw = LeapTarget.pokeTargets[i].width();
			rh = LeapTarget.pokeTargets[i].height();
						
			var rz = LeapDisplay.centerZ - pointable.screenZ;
						
			LeapTarget.pokeTargets[i].css('box-shadow', 'inset 0 0 0px #000000');
			
			if(x > rx && x < rx + rw && y > ry && y < ry + rh && rz > 0)
			{
				if(rz > 0) LeapTarget.pokeTargets[i].css('box-shadow', 'inset 0 0 '+rz+'px #000000');
				if(rz > 100)
				{
					LeapTarget.pokeTargets[i].trigger({
						type: "leappoke",
						message: "Leap Poke",
						draggable: LeapTarget.dragTargets[i],
						pointable: pointable
					});
					LeapTarget.pokeTargets[i].css('box-shadow', 'inset 0 0 '+rz+'px #6e6e6e');
					return true;
				}
			}
		}
		return false;
	}
}

